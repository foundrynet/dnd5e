# This Repository Has Moved
Foundry Virtual Tabletop has moved its source code repositories to GitHub. This is no longer the correct origin URL for the `dnd5e` repository. The new correct origin URL is https://github.com/foundryvtt/dnd5e. Please execute the following commands:

```
git remote set-url origin https://github.com/foundryvtt/dnd5e.git
git fetch --all
git reset --hard origin/master
```